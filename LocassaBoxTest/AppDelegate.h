//
//  AppDelegate.h
//  LocassaBoxTest
//
//  Created by Malkic Kevin on 29/01/2015.
//  Copyright (c) 2015 Malkic Kevin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

