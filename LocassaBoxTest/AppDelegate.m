//
//  AppDelegate.m
//  LocassaBoxTest
//
//  Created by Malkic Kevin on 29/01/2015.
//  Copyright (c) 2015 Malkic Kevin. All rights reserved.
//

#import "AppDelegate.h"
#import "LBTViewController.h"
#import "LBTBoxDataSource.h"

@interface AppDelegate ()

@end



@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.window.rootViewController = [[LBTViewController alloc] initWithDataSourceBlock:^id<LBTDataSourceProtocol>{
        
        return [[LBTBoxDataSource alloc] initWithItemCount:10];
        
    }];
    
    [self.window makeKeyAndVisible];
    
    return YES;
}





- (void)applicationWillResignActive:(UIApplication *)application {
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    
}

@end
