//
//  LBTDataSourceProtocol.h
//  LocassaBoxTest
//
//  Created by Malkic Kevin on 29/01/2015.
//  Copyright (c) 2015 Malkic Kevin. All rights reserved.
//

#import "LBTItemProtocol.h"

@protocol LBTDataSourceProtocol <NSObject>

@required

- (instancetype)initWithItemCount:(NSUInteger)itemCount;

- (NSInteger)numberOfSections;

- (NSInteger)numberOfItemsInSection:(NSInteger)section;

- (id<LBTItemProtocol>)itemAtIndexPath:(NSIndexPath*)indexPath;

- (void)moveItemFromIndexPath:(NSIndexPath*)fromIndexPath toIndexPath:(NSIndexPath*)toIndexPath;

- (NSIndexPath*)lastIndexPath;

@end
