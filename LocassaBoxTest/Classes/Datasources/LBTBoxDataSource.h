//
//  LBTBoxDataSource.h
//  LocassaBoxTest
//
//  Created by Malkic Kevin on 29/01/2015.
//  Copyright (c) 2015 Malkic Kevin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LBTDataSourceProtocol.h"

@interface LBTBoxDataSource : NSObject <LBTDataSourceProtocol>

@end
