//
//  LBTBoxDataSource.m
//  LocassaBoxTest
//
//  Created by Malkic Kevin on 29/01/2015.
//  Copyright (c) 2015 Malkic Kevin. All rights reserved.
//

#import "LBTBoxDataSource.h"
#import "LBTBoxItem.h"

@interface LBTBoxDataSource ()

@property (nonatomic, assign) NSUInteger boxCount;

@property (nonatomic, strong) NSMutableArray *boxItems;

@end


@implementation LBTBoxDataSource

- (instancetype)initWithItemCount:(NSUInteger)itemCount {
    self = [super init];
    if (self) {
        _boxCount = itemCount;
        [self generaBoxes];
    }
    return self;
}

- (NSMutableArray *)boxItems {
    if (!_boxItems) {
        _boxItems = [NSMutableArray new];
    }
    return _boxItems;
}

- (void)generaBoxes {
    for (NSUInteger i = 0; i < self.boxCount; i++) {
        LBTBoxItem *boxItem = [[LBTBoxItem alloc] initWithSize:CGSizeMake(100, 100)];
        [self.boxItems addObject:boxItem];
    }
}

#pragma mark - LBTDataSourceProtocol

- (NSInteger)numberOfSections {
    return 1;
}

- (NSInteger)numberOfItemsInSection:(NSInteger)section {
    return self.boxCount;
}

- (LBTBoxItem*)itemAtIndexPath:(NSIndexPath*)indexPath {
    return self.boxItems[indexPath.item];
}

- (void)moveItemFromIndexPath:(NSIndexPath*)fromIndexPath toIndexPath:(NSIndexPath*)toIndexPath {
    LBTBoxItem *boxItem = self.boxItems[fromIndexPath.item];
    [self.boxItems removeObject:boxItem];
    [self.boxItems insertObject:boxItem atIndex:toIndexPath.item];
}

- (NSIndexPath*)lastIndexPath {
    return [NSIndexPath indexPathForItem:self.boxItems.count-1 inSection:0];
}

@end
