//
//  LBTView.m
//  LocassaBoxTest
//
//  Created by Malkic Kevin on 29/01/2015.
//  Copyright (c) 2015 Malkic Kevin. All rights reserved.
//

#import "LBTView.h"

@interface LBTView ()

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, assign) CGFloat spacing;

@end


@implementation LBTView

- (instancetype)initWithItemSpacing:(CGFloat)spacing
{
    self = [super init];
    if (self) {
        _spacing = spacing;
    }
    return self;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *collectionViewFlowLayout = [[UICollectionViewFlowLayout alloc] init];
        collectionViewFlowLayout.minimumInteritemSpacing = self.spacing;
        collectionViewFlowLayout.minimumLineSpacing = self.spacing;
        _collectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:collectionViewFlowLayout];
        _collectionView.contentInset = UIEdgeInsetsMake(20.0f, 0.0f, 0.0f, 0.0f);
        _collectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _collectionView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_collectionView];
    }
    return _collectionView;
}

@end
