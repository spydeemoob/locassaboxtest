//
//  ViewController.h
//  LocassaBoxTest
//
//  Created by Malkic Kevin on 29/01/2015.
//  Copyright (c) 2015 Malkic Kevin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LBTDataSourceProtocol.h"

@interface LBTViewController : UIViewController

- (instancetype)initWithDataSourceBlock:(id<LBTDataSourceProtocol> (^)())dataSourceBlock;

@end

