//
//  ViewController.m
//  LocassaBoxTest
//
//  Created by Malkic Kevin on 29/01/2015.
//  Copyright (c) 2015 Malkic Kevin. All rights reserved.
//

#import "LBTViewController.h"
#import "LBTView.h"
#import "LBTBoxItem.h"


static NSString *reuseIdentifier = @"BoxCell";


@interface LBTViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) LBTView *mainView;

@property (nonatomic, strong) id<LBTDataSourceProtocol> dataSource;

@end


@implementation LBTViewController

- (instancetype)initWithDataSourceBlock:(id<LBTDataSourceProtocol> (^)())dataSourceBlock {
    self = [super init];
    if (self) {
        if (dataSourceBlock) {
            _dataSource = dataSourceBlock();
        }
    }
    return self;
}

- (void)loadView {
    self.view = [[LBTView alloc] initWithItemSpacing:10];
}

- (LBTView *)mainView {
    return (LBTView*)self.view;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.mainView.collectionView registerClass:UICollectionViewCell.class forCellWithReuseIdentifier:reuseIdentifier];
    self.mainView.collectionView.delegate = self;
    self.mainView.collectionView.dataSource = self;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [self.dataSource numberOfSections];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.dataSource numberOfItemsInSection:section];
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    LBTBoxItem *item = [self.dataSource itemAtIndexPath:indexPath];
    
    cell.backgroundColor = item.backgroundColor;
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    NSIndexPath *toIndexPath = [self.dataSource lastIndexPath];    
    [self.dataSource moveItemFromIndexPath:indexPath toIndexPath:toIndexPath];
    [collectionView moveItemAtIndexPath:indexPath toIndexPath:toIndexPath];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [[self.dataSource itemAtIndexPath:indexPath] sizeOfItem];
}


@end
