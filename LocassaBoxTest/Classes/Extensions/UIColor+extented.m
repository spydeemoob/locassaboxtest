//
//  UIView+extented.m
//  LocassaBoxTest
//
//  Created by Malkic Kevin on 29/01/2015.
//  Copyright (c) 2015 Malkic Kevin. All rights reserved.
//

#import "UIColor+extented.h"

@implementation UIColor (extented)

+ (UIColor *)randomColor {
    return [UIColor colorWithRed:(arc4random_uniform(255)/255.0) green:arc4random_uniform(255)/255.0 blue:arc4random_uniform(255)/255.0 alpha:1];
}

@end
