//
//  LBTBoxItem.m
//  LocassaBoxTest
//
//  Created by Malkic Kevin on 29/01/2015.
//  Copyright (c) 2015 Malkic Kevin. All rights reserved.
//

#import "LBTBoxItem.h"
#import "UIColor+extented.h"

@interface LBTBoxItem ()

@property (nonatomic, assign) CGSize size;

@property (nonatomic, strong) UIColor *backgroundColor;

@end


@implementation LBTBoxItem

- (instancetype)initWithSize:(CGSize)size {
    self = [super init];
    if (self) {
        _size = size;
        _backgroundColor = [UIColor randomColor];
    }
    return self;
}

- (CGSize)sizeOfItem {
    return self.size;
}

@end
