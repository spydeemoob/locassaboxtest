//
//  LBTItemProtocol.h
//  LocassaBoxTest
//
//  Created by Malkic Kevin on 29/01/2015.
//  Copyright (c) 2015 Malkic Kevin. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>

@protocol LBTItemProtocol <NSObject>

@required

- (instancetype)initWithSize:(CGSize)size;

- (CGSize)sizeOfItem;

@end
