//
//  LBTBoxItem.h
//  LocassaBoxTest
//
//  Created by Malkic Kevin on 29/01/2015.
//  Copyright (c) 2015 Malkic Kevin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <UIKit/UIKit.h>

#import "LBTItemProtocol.h"

@interface LBTBoxItem : NSObject <LBTItemProtocol>

@property (nonatomic, strong, readonly) UIColor *backgroundColor;

@end
